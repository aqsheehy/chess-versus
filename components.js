var cfg = require('./config');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var app = express();

var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var IrcClient = require("irc").Client;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use('/', routes);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

var irc = new IrcClient("irc.twitch.tv", cfg.ThisAccount, {
    channels: [ ["#" + cfg.ThisAccount, "#" + cfg.OpponentAccount] + " " + cfg.AuthToken ],
    debug: false,
    password: cfg.AuthToken,
    username: cfg.ThisAccount
});

var socket = null;

irc.connect();

io.on('connection', function(s){

  socket = s;

  socket.on('sendMessage', function(m){
    console.log("sendMessage: " + JSON.stringify(m));
    irc.say(m.to, m.message);
  });
  
});

irc.addListener('message', function (f, t, m) {
  if (!m || m[0] != '!' || !socket) return;
  var args = { from: f, to: t, message: m };
  console.log("messageReceived: " + JSON.stringify(args));
  socket.emit('messageReceived', args);
});

irc.addListener('error', console.log);

module.exports = { 
  app: app,
  server: server,
  io: io,
  irc: irc
};
