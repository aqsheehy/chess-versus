(function(hub){
	'use strict';

	var turn = 'white';
	var colour = null;
	var team = null;
	var opponent = null;

	var SECONDS = 1000;
	var VOTEPROCESS = 30;

	var votes = {}; // UserId | Move

	hub.subscribe('opponentMoved', function(){

		votes = {};
		turn = colour;
		_workerWait();

	});

	hub.subscribe('voteCast', function(v){

		votes = votes || {};
		votes[v.from] = v.vote;

	});

	hub.subscribe('newGame', function(newGame){

		colour = newGame.colour;
		team = newGame.team;
		opponent = newGame.opponent;
		_workerWait();

	});

	hub.subscribe('gameOver', function(){

		hub.request('getMoveHistory', {}, function(history){

			_slowBack(history);

		});

	});

	function _workerWait(){

		setTimeout(_selectionWorker, VOTEPROCESS * SECONDS);	

	}

	function _selectionWorker(){

		if (turn != colour) return console.log('worker skipped');
		console.log('worker processing');

		var raw = _.values(votes);

		if (!raw || raw.length == 0) return _workerWait();

		var tallied = _.groupBy(raw, function(v){ return v });
		var morphed = _.map(tallied, function(v, k){ return { vote: k, count: v.length }; });
		var sorted = _.sortBy(morphed, function(t) { return t.count; }).reverse();
		var winner = sorted[0].vote;

		votes = {};

		hub.publish('moveSelected', { move: winner });

		turn = colour == 'white' ? 'black' : 'white';
	}

	function _slowBack(moves){

		setTimeout(function(){ 

			if (!moves || moves.length == 0) 
				return hub.publish('newGame', {  
					colour: colour == 'white' ? 'black' : 'white',
					team: team,
					opponent: opponent
				});

			var temp = new Chess();
			for (var i = 0; i < moves.length; i++)
				temp.move(moves[i]);

			hub.publish('fenChanged', temp.fen());

			_slowBack(moves.slice(0, moves.length - 1));

		}, 1000);
		
	}

	

})(air.hub('cv'));