(function(hub){
	'use strict';

	var logic = null;
	var colour = null;
	var turn = null;

	hub.replyTo('getMoveHistory', function(t, reply){
		if (!logic) return reply(null);
		reply(logic.history());
	});

	hub.replyTo('getAvailableMoves', function(t, reply){
		if (!logic) return reply(null);
		reply(logic.moves());
	});

	hub.replyTo('getTurn', function(t, reply){
		if (!logic) return reply(null);
		reply(logic.turn() == 'w' ? 'white' : 'black');
	});

	hub.subscribe('newGame', function(newGame){

		logic = new Chess();
		colour = newGame.colour;
		turn = 'white';
		hub.publish('colourChanged', colour);
		hub.publish('fenChanged', logic.fen());
		hub.publish('turnChanged', turn);

	});

	function _makeMove(mv, t){

		var move = mv.move;
		var result = logic.move(move);
		if (!result) return;
		hub.publish('fenChanged', logic.fen());
		if (logic.game_over()) return hub.publish('gameOver', logic.turn());

		turn = t;		
		hub.publish('turnChanged', turn);

	}

	hub.subscribe('moveSelected', function(v){
		_makeMove(v, colour == 'white' ? 'black' : 'white');
	});

	hub.subscribe('opponentMoved', function(v){
		_makeMove(v, colour);
	});

})(air.hub('cv'));