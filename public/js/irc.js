(function(hub){

	var opponentAccount = getParameterByName('opponent');
	var thisAccount = getParameterByName('this');

	var socket = io.connect('ws://localhost:3000');

	hub.subscribe('moveSelected', function(mv){
		socket.emit('sendMessage', { to: '#' + thisAccount, message: '!moved ' + mv.move });
	});

	socket.on('messageReceived', function(m){

		var message = m.message;
		var parts = message.split(' ');

		switch (parts[0]){
			case '!moved': // When the opponent moved.
				if (m.from.toLowerCase() != opponentAccount) return;
				return hub.publish('opponentMoved', { move: parts[1] });
			default:
				if (m.to != '#' + thisAccount) return;
				return hub.request('getAvailableMoves', {}, function(availableMoves){
					var move = parts[0].substring(1); // !Ne5 => Ne5
					if (availableMoves.indexOf(move) > -1)
						return hub.publish('voteCast', { from: m.from, vote: move }); 
				});
				
		}

	});

})(air.hub('cv'));